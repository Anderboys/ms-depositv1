package com.ander.service;


import com.ander.dao.ITransactionDao;
import com.ander.domain.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;


@Service
public class TransactionServiceImpl implements ITransactionService {


    // Inyectamos el Repositorio
    @Autowired
    private ITransactionDao _transactionDao;



    // Iyectamos q es de tipo Transaction
    @Override
    @Transactional
    public Transaction save(Transaction transaction) {
        return _transactionDao.save(transaction);
    }



}
