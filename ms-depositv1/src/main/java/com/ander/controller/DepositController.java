package com.ander.controller;
import com.ander.domain.Transaction;
import com.ander.producer.DepositEventProducer;
import com.ander.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DepositController {
    @Autowired
    private ITransactionService service;
    @Autowired
    DepositEventProducer eventProducer;
    private Logger log = LoggerFactory.getLogger(DepositController.class);

    @PostMapping("depositevent")
    public ResponseEntity<Transaction> postDepositEvent(@RequestBody Transaction transaction) throws JsonProcessingException {
        log.info("antes de transql");
        Transaction transql = service.save(transaction);
        log.info("despues de transql");
        log.info("antes de sendDepositEvent");
        eventProducer.sendDepositEvent(transql);
        log.info("despues de sendDepositEvent");
        return ResponseEntity.status(HttpStatus.CREATED).body(transql);
    }

 

}
