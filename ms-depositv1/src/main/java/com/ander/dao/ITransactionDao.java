package com.ander.dao;

import com.ander.domain.Transaction;
import org.springframework.data.repository.CrudRepository;

public interface ITransactionDao extends CrudRepository<Transaction, Integer> {


}
