package com.ander.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class AutoCreateConfig {
	
	  // PARA LLEGAR A KAFKA NECESITAMOS UN TOPICO
    // para poder enviar los mensajes hay que  Crear 1 Topico y sus particiones en kafka
    // CREANDO Topico y sus particiones en kafka
    @Bean
    public NewTopic depositEvent() {
        return TopicBuilder.name("transaction-events").partitions(3).replicas(1).build();
    }

}
