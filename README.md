# ms-depositv1
- IDe Spring Tools
- JDK 11.0.15

https://www.youtube.com/watch?v=qDfuxRfsp0U

- Detalle:
- **Para que pueda ejecutar el proyecto local debe cambiar las configuraciones
del application.properties al local ejm:**
```
#KAFKA
spring.kafka.producer.bootstrap-servers=localhost:9092
#spring.kafka.producer.bootstrap-servers=servicekafka:9092
spring.kafka.admin.properties.bootstrap.servers=localhost:9092
#spring.kafka.admin.properties.bootstrap.servers=servicekafka:9092
#CONEXION Postgress
spring.datasource.url=jdbc:postgresql://localhost:5432/db_account
#spring.datasource.url=jdbc:postgresql://microservicio-postgres12:5432/db_account
```
- **y cambiar el archivo server.properties del servicekafka**
```
y modificar
adversited.listener=PLAINTEXT://servicekafka:9092
por
adversited.listener=PLAINTEXT://localhost:9092
```


- **Ya que las configuraciones de este proyecto esta directamente para dockerizar**



